#!/usr/bin/env bash

# Set up firewall and limit ssh
sudo apt install -y ufw
sudo ufw limit ssh
sudo ufw allow 8200/tcp	# MiniDLNA
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw --force enable
