#!/usr/bin/env bash

# Download and install veracrypt
sudo wget https://launchpad.net/veracrypt/trunk/1.26.7/+download/veracrypt-1.26.7-Debian-12-amd64.deb
sudo apt install -y ~/veracrypt-1.26.7-Debian-12-amd64.deb

# Remove deb package
sudo rm ~/veracrypt-1.26.7-Debian-12-amd64.deb
