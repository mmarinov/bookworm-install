#!/bin/bash

# Dismount all drives
sudo veracrypt -t -d

# Mount vault
sudo veracrypt -t -k "" --pim=0 --protect-hidden=no /mnt/storage/vault /mnt/secret

# List mounted drives
sudo veracrypt -t -l

# Restart miniDLNA
sudo systemctl restart minidlna.service

exit 0
