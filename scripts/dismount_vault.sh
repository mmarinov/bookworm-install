#!/bin/bash

# Dismount vault
sudo veracrypt -t -d /mnt/secret

# Restart miniDLNA
sudo systemctl restart minidlna.service

exit 0
