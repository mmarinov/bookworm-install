#!/usr/bin/env bash

# Set up MiniDLNA
sudo apt install -y minidlna

sudo cp -f ./configs/minidlna.conf /etc
sudo cp -f ./services/minidlna.service /usr/lib/systemd/system

sudo chown root:root /etc/minidlna.conf
sudo chown root:root /usr/lib/systemd/system/minidlna.service

sudo systemctl daemon-reload
sudo systemctl restart minidlna.service
