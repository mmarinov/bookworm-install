#!/usr/bin/env bash

# Install packages after installing base Debian 12 with no GUI
# Choose one of the desktop environments, comment out the others

# Install GNOME minimal desktop environment
# sudo apt install -y gnome-core
# sudo -u Debian-gdm dbus-run-session gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'logout'

# Install XFCE4 minimal desktop environment
# sudo apt install -y xfce4 xfce4-goodies

# Install KDE Plasma minimal desktop environment
sudo apt install -y kde-plasma-desktop

# Install additional packages
sudo apt install -y neofetch wget htop firefox-esr okular vlc gwenview unzip

# Support for StampIT e-signature token
sudo apt install -y opensc pcscd

# Fonts
sudo apt install -y fonts-recommended fonts-font-awesome fonts-terminus

# Create mount points
sudo mkdir /mnt/storage
sudo mkdir /mnt/secret
echo "/dev/disk/by-uuid/23C8DD942464A4E7 /mnt/storage ntfs-3g uid=1000,gid=100,fmask=0113,dmask=0002 0 0" | sudo tee -a /etc/fstab
sudo systemctl daemon-reload
sudo mount /mnt/storage

# Set up firewall
bash ./ufw.sh

# Install veracrypt
bash ./veracrypt.sh

# Set up MiniDLNA media server
bash ./minidlna.sh

# Copy custom scripts
mkdir ~/bin
cp ./scripts/*.sh ~/bin
