# bookworm-install


## Description
My scripts to complete a minimal Debian 12 installation.

### Desktop environments
- gnome-core
- xfce4 xfce4-goodies
- kde-plasma-desktop

### Packages
- neofetch
- wget
- htop
- minidlna
- ufw
- firefox-esr
- okular
- vlc
- gwenview
- unzip

### Local installs
- veracrypt-1.26.7-Debian-12-amd64.deb

## Installation
1. Install Debian 12 without desktop environment.
2. Login to terminal and run:
	```
	sudo apt update && sudo apt upgrade
	sudo apt install -y git
	git clone https://gitlab.com/mmarinov/bookworm-install.git
	```
3. Run the installer.
	```
	cd ~/bookworm-install
	./installer.sh
	```

## Usage
Comment/uncomment lines as you find necessary.

## Acknowledgment
Inspired by [JustAGuyLinux](https://github.com/drewgrif)'s [bookworm-scripts](https://github.com/drewgrif/bookworm-scripts).
